script
	declare_show_me															;This is a show me script
	
	declare_counter abandon													;Track whether the settlement scroll is displayed

	monitor_event ScrollClosed ScrollClosed own_settlement_info_scroll		;Catch the scroll closing
		set_counter abandon 1
	end_monitor

	monitor_event ScrollOpened TrueCondition
		set_counter abandon 1
	end_monitor
	
	monitor_event AbandonShowMe TrueCondition								;Catch the AbandonShowMe event
		set_counter abandon 1
	end_monitor

	monitor_event ScrollAdviceRequested TrueCondition				; Catch the advice being requested again
		set_counter abandon 1
	end_monitor

	monitor_event SettlementScrollAdviceRequested TrueCondition				;Catch the advice being requested again
		set_counter abandon 1
	end_monitor

	monitor_conditions I_CompareCounter abandon = 1							;Catch the show me being abandoned for whatever reason
		ui_flash_stop														;Stop the UI highlighting
		suspend_unscripted_advice false										;Restore unscripted advice
		enable_entire_ui
		terminate_script													;All done
	end_monitor

	if I_ScrollOpen own_settlement_info_scroll

		dismiss_advice															;Gets rid of settlement help intro advice
		while I_AdvisorVisible													;While the advisor is on screen...
		end_while
	
		suspend_unscripted_advice true											;Don't display any advice that isn't part of this show me script
		disable_ui settlement_info_retrain_tab
		disable_ui settlement_info_construction_tab
		disable_ui settlement_info_repair_tab
		disable_ui show_construction_advice_button
		disable_ui building_browser_button
		
		;play show me recruitment advice
		advance_advice_thread Show_Me_Recruitment_Advice_Thread	
		
		;highlight recruitment items
		ui_flash_start available_training_options
		
		wait 2
		ui_flash_stop
		
		;highlight recruitment queue
		ui_flash_start recruitment_queue	
		
		wait 4
		ui_flash_stop
		
		wait 5
		
		;highlight adviser portrait
		ui_flash_start show_construction_advice_button
			
		while I_AdvisorSpeechPlaying
			and I_AdvisorVisible							;Keep UI highlighting and advice on screen until dismissed by player or audio finished
		end_while	
		
		ui_flash_stop
				
		suspend_unscripted_advice false						;Restore unscripted advice
		enable_entire_ui
	end_if
	
end_script